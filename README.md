# Antivirus

This code analyze a file and sanitize it if it is infected with our sample virus.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

First, you need to clone the repository

```
git clone git@bitbucket.org:silvia_silvia05/antivirus.git
```

### Installing

You can install the requirements using pipenv

```
pipenv install
```

And start the virtual environment

```
pipenv shell
```

## Built With

* [Pipenv](http://www.dropwizard.io/1.0.2/docs/) - Pipenv: Flujo de trabajo en Python para humanos